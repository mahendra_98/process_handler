package com.infrrd.assignment.concurrent.processing.utils;

import lombok.Getter;

@Getter
public enum ProcessStatus {
	START, INTERRUPTED, STOPPED;
}
