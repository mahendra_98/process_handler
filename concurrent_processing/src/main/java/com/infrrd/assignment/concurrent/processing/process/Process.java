package com.infrrd.assignment.concurrent.processing.process;

import com.infrrd.assignment.concurrent.processing.utils.ProcessStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * This Process class is used for calling the processes
 * @author mahendra
 *
 */
@Setter
@Getter
public class Process {

	private Integer processId;
	private ProcessStatus processStatus;

	public Process(Integer processId) {
		this.processId = processId;
		this.processStatus=ProcessStatus.START;
	}
	
	/**
	 * Required different process can be called here and they can individually
	 * process there tasks
	 *
	 */
	
}
