package com.infrrd.assignment.concurrent.processing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConcurrentProcessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConcurrentProcessingApplication.class, args);
	}

}
