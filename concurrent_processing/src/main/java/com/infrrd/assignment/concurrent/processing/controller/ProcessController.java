package com.infrrd.assignment.concurrent.processing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infrrd.assignment.concurrent.processing.models.GenericResponse;
import com.infrrd.assignment.concurrent.processing.models.ProcessRequest;
import com.infrrd.assignment.concurrent.processing.service.ProcessService;

/**
 * @author mahendra 
 * Controller class for Procesess
 */

@RestController
@RequestMapping(value = "v1/process")
public class ProcessController {

	@Autowired
	private ProcessService processService;

	/**
	 * This API starts process defined from given process id
	 * @param Process Request
	 * @return response message
	 */
	
	@PostMapping("start")
	public GenericResponse startProcess(@RequestBody ProcessRequest request) {
		return processService.startProcess(request);
	}
	
	/**
	 * This API Stops process for given process id
	 * @param processId
	 * @return
	 */
	@GetMapping("stop/{processId}")
	public GenericResponse stopProcess(@PathVariable Integer processId) {
		return processService.stopProcess(processId);
	}

}
