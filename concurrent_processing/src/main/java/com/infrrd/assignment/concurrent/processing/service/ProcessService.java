package com.infrrd.assignment.concurrent.processing.service;

import com.infrrd.assignment.concurrent.processing.models.GenericResponse;
import com.infrrd.assignment.concurrent.processing.models.ProcessRequest;

public interface ProcessService {

	GenericResponse startProcess(ProcessRequest request);

	GenericResponse stopProcess(Integer processId);

}
