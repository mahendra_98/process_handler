package com.infrrd.assignment.concurrent.processing.service.impl;

import java.util.concurrent.ConcurrentHashMap;

import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

import com.infrrd.assignment.concurrent.processing.models.GenericResponse;
import com.infrrd.assignment.concurrent.processing.models.ProcessRequest;
import com.infrrd.assignment.concurrent.processing.process.Process;
import com.infrrd.assignment.concurrent.processing.service.ProcessService;
import com.infrrd.assignment.concurrent.processing.utils.ProcessStatus;

/**
 * @author mahenndra
 * Implementation class for prcoess related operations 
 */
@Service
@Log4j2
public class PrcocessServiceImpl implements ProcessService {

	private final ConcurrentHashMap<Integer, Process> process = new ConcurrentHashMap<>();

	@Override
	public GenericResponse startProcess(ProcessRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			if (process.containsKey(request.getProcessId())) {
				Process currentProcess = process.get(request.getProcessId());
				if (currentProcess.getProcessStatus().equals(ProcessStatus.INTERRUPTED) || currentProcess.getProcessStatus().equals(ProcessStatus.STOPPED)) {
					currentProcess.setProcessStatus(ProcessStatus.START);
					 response = new GenericResponse("Process re-started.");
					log.info("Process re-started for Process id {}.", request.getProcessId());
				} else {
					currentProcess.setProcessStatus(ProcessStatus.INTERRUPTED);
					 response = new GenericResponse("Process interupted.");
					 log.info("Process interupted for Process id {}.", request.getProcessId());
				}
			} else {
				process.put(request.getProcessId(),new Process(request.getProcessId()));
				response = new GenericResponse("Process started.");
				 log.info("Process started for Process id {}.", request.getProcessId());
			}
		} catch (Exception e) {
			log.error(e);
		}
		return response;
	}

	@Override
	public GenericResponse stopProcess(Integer processId) {
		GenericResponse response = new GenericResponse();
		try {
			if (process.containsKey(processId)) {
				Process currentProcess = process.get(processId);
				if(currentProcess.getProcessStatus().equals(ProcessStatus.INTERRUPTED)){
					response=new GenericResponse("Already interupted.");
					log.info("Process already interupted for Process id {}.", processId);
				}else {
					currentProcess.setProcessStatus(ProcessStatus.STOPPED);
					response = new GenericResponse("Process stopped.");
					log.info("Process stopped for Process id {}.", processId);
				}
			}else{
				response = new GenericResponse("Process does not exist.");
				log.info("Process does not exist for Process id {}.", processId);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return response;
	}
}
