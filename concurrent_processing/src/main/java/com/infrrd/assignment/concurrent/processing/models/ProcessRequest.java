package com.infrrd.assignment.concurrent.processing.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessRequest {
	private Integer processId;
}
